package kodut66;

public class Seis {
	
	 int elusid = 7;
	 boolean[] t2htiLeitud;
	 String s6na;
	 
	public Seis(String s6na) {
		this.s6na = s6na;
		t2htiLeitud = new boolean[s6na.length()];
	}
	
	public String tagastaHetkeseis(){
		String hetkeSeis = "";
		for(int i = 0; i < t2htiLeitud.length; i++){
			if(t2htiLeitud[i]){
				hetkeSeis += s6na.charAt(i) +" ";
				
			}
			else {
				hetkeSeis += "_ ";
			}
		}
		hetkeSeis += '\n'+ "Elusid: " + elusid;
		return hetkeSeis;
	} 
	
	public boolean kontrolliPakkumist(char c) {
		boolean arvatiMidagi = false;
		for(int i = 0; i < t2htiLeitud.length; i++){
			if(t2htiLeitud[i]!= true){
				if(c == s6na.charAt(i)){
					t2htiLeitud[i] = true;
					arvatiMidagi = true;
				}
				 
			}
			
		}
		return arvatiMidagi;
	}
	
	public boolean onL6petatud(){
		for(boolean b : t2htiLeitud){
			if(!b){
				return false ;
			}
			
		}
		System.out.println("V�ITSID!!!");
		return true;
	}

}
