package kodut66;

import java.util.Scanner;

public class KasutajaSuhtlus {

	Scanner kasutajaSisestus = new Scanner(System.in);

	public String kysiS6na() {
		System.out.println("Palun sisesta s�na.");
		String s6na = kasutajaSisestus.nextLine();

		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();

		return s6na;
	}

	public String kysiPakkumine() {

		System.out.println("Palun sisesta pakkumiseks t�ht.");
		String c = kasutajaSisestus.nextLine();

		while (c.length() != 1) {
			System.out.println("Sisestasid rohkem kui �he t�he v�i ei pakkunud midagi, proovi uuesti.");
			c = kasutajaSisestus.nextLine();
		}
		return c;
	}
}
